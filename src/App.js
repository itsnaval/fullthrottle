import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from "./components/Home";
import * as firebaseConfig from 'firebase';
const config = {
  apiKey: "AIzaSyCBP9ZGHOeFX_xgWJUuEoUh37-R8FSe6O8",
  authDomain: "fullthrottle-20639.firebaseapp.com",
  databaseURL: "https://fullthrottle-20639.firebaseio.com",
  projectId: "fullthrottle-20639",
  storageBucket: "fullthrottle-20639.appspot.com",
  messagingSenderId: "140805927768",
  appId: "1:140805927768:web:e7fc0048ebde4e8c106b2d"
};
firebaseConfig.initializeApp(config);

function App() {
  return (
    <div className="App">
      <div className="dis-flex">
				<Home />
			</div>
    </div>
  );
}

export default App;
