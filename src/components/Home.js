import React, { Component } from "react";
import axios from "axios";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import NavBar from "./NavBar";
import "./Home.css";
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'

import Form from 'react-bootstrap/Form'

class App extends Component {
    state = {
        amount: 500,
        months: 6,
        dpay: 500,
        interestRate: 0,
        monthlyPayment: 0,
        numPayments: 0
    };

    componentDidMount() {
        axios
            .get(
                `https://ftl-frontend-test.herokuapp.com/interest?amount=${this.state.amount}&numMonths=${this.state.months}`
            )
            .then(res => {
                this.setState({
                    interestRate: res.data.interestRate,
                    monthlyPayment: res.data.monthlyPayment.amount,
                    numPayments: res.data.numPayments,
                    dpay: 500,
                });
            })
            .catch(e => console.log(e));
        console.log(this.state);
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            this.state.amount !== prevState.amount ||
            this.state.months !== prevState.months ||
            this.state.dpay !== prevState.dpay

        ) {
            axios
                .get(
                    `https://ftl-frontend-test.herokuapp.com/interest?amount=${
					this.state.amount
					}&numMonths=${this.state.months}`
                )
                .then(res => {
                    console.log(res.data);
                    if (res.data.status && res.data.status === "error") {
                        console.log("Error occurred");
                    } else {
                        this.setState({
                            interestRate: res.data.interestRate,
                            monthlyPayment: res.data.monthlyPayment.amount,
                            numPayments: res.data.numPayments
                        });
                    }
                })
                .catch(e => console.log(e));
        }
    }

    formatAmountLabel = val => {
        return `$${val}`;
    };

    handleFormSubmit = () => {
        localStorage.setItem('data', JSON.stringify(this.state));
    };

    render() {

        return ( 
            < >
            <NavBar / >
            <div className = "container w-100 card" >

            <form onSubmit = { this.handleFormSubmit } >
            <div className = "form-group" >
            <label className="llabel"> Loan Amount </label> 
            <InputRange maxValue = { 5000 }
            minValue = { 500 }
            value = { this.state.amount }
            onChange = { amount => this.setState({ amount }) }
            formatLabel = { this.formatAmountLabel }
            /> 
            </div > 
            <br >
            </br>
            <div className = "form-group" > 
            <label className="llabel">Loan Duration</label> 
            <InputRange maxValue = { 24 }
            minValue = { 6 }
            value = { this.state.months }
            onChange = { months => this.setState({ months }) }
            /> 
            </div > 
            <br >
            </br>

            <div className = "form-group" > 
            <label className="llabel">Monetary Amount</label>
            <InputRange maxValue = { 5000 }
            minValue = { 500 }
            value = { this.state.dpay }
            onChange = { dpay => this.setState({ dpay }) }
            /> 
            </div >





            <br />

            <Table striped bordered hover size = "sm" >
            <thead>
            <tr >
            <th > Interest Rate: </th> 
            <th > < span className = "interest-display data-display" > $ { this.state.interestRate } </span></th >
            </tr> 
            <tr >
            <th > Monthly Payment: </th> 
            <th > < span className = "payment-display data-display" > $ { this.state.monthlyPayment } </span></th >
            </tr> 
            <tr >
            <th > Number of Payments: </th> 
            <th > < span className = "number-display data-display" > { this.state.numPayments } </span></th >
            </tr> 
            <tr >
            <th > Total Payment: </th> 
            <th > < span className = "number-display data-display" > $ {
                ((this.state.amount - this.state.dpay) / this.state.months)*(this.state.interestRate)
            } </span></th >
            </tr> 
            </thead >

            </Table> 

            <Button type = "submit" variant = "primary" > Save </Button>

            </form>

            </div> 
            </>
        );
    }
}

export default App;