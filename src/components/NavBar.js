import React from "react";
import Table from 'react-bootstrap/Table'

export default function Navbar() {

	let comments = JSON.parse(localStorage.getItem('data'));
	if(comments==null){
		comments={
			amount: 500,
        months: 6,
        dpay: 0,
        interestRate: 0,
        monthlyPayment: 0,
        numPayments: 0
		}
	}
	return (
		<nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-5">
			<div className="container d-flex justify-content-center">
				<a className="navbar-brand" href="/">
					<img src={require('../assets/fullthrottle.png')} />
					<br></br><br></br>
					<Table striped bordered hover size="xm" style={{fontSize:'14px',color:'white'}}>
						<thead>
						<tr>
								<td>Loan Amount:</td>
								<td><b>${comments.amount}</b></td>
							</tr>
							<tr>
								<td>Duration:</td>
								<td><b>{comments.months}</b></td>
							</tr>
							<tr>
								<td>Monetary Amount:</td>
								<td><b>${comments.interestRate}</b></td>
							</tr>
							<tr>
								<td>Interest Rate:</td>
								<td><b>${comments.interestRate}</b></td>
							</tr>
							<tr>
								<td>Monthly Payment:</td>
								<td><b>${comments.monthlyPayment}</b></td>
							</tr>
							<tr>
								<td>Number of Payments:</td>
								<td><b>{comments.numPayments}</b></td>
							</tr>
							<tr>
								<td>Total Payment:</td>
								<td><b>${((comments.amount - comments.dpay) / comments.months)*(comments.interestRate)}</b></td>
							</tr>
						</thead>

					</Table>
					<br></br>
					<footer>
					<small style={{fontSize:'12px',float:'right',color:'yellow'}}>Version 0.0.1</small>
					</footer>
				</a>
			</div>
		</nav>
	);
}
