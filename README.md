
### Live here: https://fullthrottle-project.herokuapp.com/


### Installing

Follow these steps to run this project in your local computer.

```
$ https://gitlab.com/itsnaval/fullthrottle.git
$ cd fullthrottle
$ npm i
$ npm install axios
$ npm i react-input-range
$ npm i react-bootstrap

```

Now, to run the project on port `3000`, run:

```
$ npm start
```

Go to `http://localhost:3000` to view the app.

## Built With

- [React.JS] -Frontend library used in the project.
- [react-bootstrap]- Used for basic styling.

## Author

- **Navalkumar Karli** - (https://gitlab.com/itsnaval/fullthrottle)
